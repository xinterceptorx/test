package online.interceptor;

import online.interceptor.util.tests.TimeExecutionLogger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;


@Tag("unit-test")
class AppTest implements TimeExecutionLogger {

    @BeforeEach
    void setUp() {
    }

    
    @Test
    void printHelloTest() {
        System.out.println("Hello Test");
    }
}