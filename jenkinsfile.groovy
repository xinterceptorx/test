pipeline {
    agent any
	environment {
		JAVA_HOME='/usr/lib/jvm/java-8-oracle'
		MAVEN_HOME='/var/lib/jenkins/tools/hudson.tasks.Maven_MavenInstallation/Maven_3.2.5'
		MAVEN_OPTS='-Xmx1024M -XX:MaxPermSize=256M'
	}
	options {
		timeout(time: 5, unit: 'MINUTES')
	}
    stages {
		stage('info') {
			steps {
				echo "Running ${env.JOB_NAME} with ID ${env.BUILD_ID} on ${env.JENKINS_URL}"
				sh 'java -version'
                sh '${MAVEN_HOME}/bin/mvn -version'
			}
		}
        stage('checkout') {
            steps {
				echo "Checking out branch: env.GIT_BRANCH"
				checkout([$class: 'GitSCM', branches: [[name: "/env.GIT_BRANCH"]], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[url: 'https://xinterceptorx@bitbucket.org/xinterceptorx/test.git']]])
            }
        }
		stage('build') {
			steps {
				echo "Building branch: env.GIT_BRANCH"
				sh '${MAVEN_HOME}/bin/mvn -Dmaven.test.failure.ignore clean package'
			}
		}
		stage('report') {
			steps {
				junit '**/target/surefire-reports/TEST-*.xml'
				archiveArtifacts 'target/*.jar'
			}
		}
    }
}